// Code generated for windows/amd64 by 'generator --cpp /usr/bin/x86_64-w64-mingw32-gcc --goarch amd64 --goos windows -DNDEBUG -DSQLITE_OMIT_SEH -DSQLITE_OS_WIN=1 -I /tmp/libsqlite3/sqlite-src-3490100 -build-lines \/\/go:build windows && (amd64 || arm64)\n\/\/ \x2bbuild windows\n\/\/ \x2bbuild amd64 arm64 -map gcc=x86_64-w64-mingw32-gcc -o mptest/ccgo_windows.go /tmp/libsqlite3/sqlite-src-3490100/mptest/mptest.c -lsqlite3', DO NOT EDIT.

//go:build windows && (amd64 || arm64)
// +build windows
// +build amd64 arm64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libsqlite3"
)

var _ reflect.Type
var _ unsafe.Pointer

const DEFAULT_TIMEOUT = 10000
const ENOENT = 2
const FALSE = 0
const MX_ARG = 2
const SEEK_END = 2
const SQLITE_BUSY = 5
const SQLITE_CONFIG_LOG = 16
const SQLITE_DONE = 101
const SQLITE_ERROR = 1
const SQLITE_FCNTL_VFSNAME = 12
const SQLITE_NOTICE = 27
const SQLITE_OK = 0
const SQLITE_OPEN_CREATE = 4
const SQLITE_OPEN_READWRITE = 2
const SQLITE_ROW = 100
const SQLITE_SCHEMA = 17
const SQLITE_UTF8 = 1

type __builtin_va_list = uintptr

type va_list = uintptr

type WORD = uint16

type DWORD = uint32

type LPBYTE = uintptr

type LPSTR = uintptr

type HANDLE = uintptr

type STARTUPINFOA = struct {
	Fcb              DWORD
	FlpReserved      LPSTR
	FlpDesktop       LPSTR
	FlpTitle         LPSTR
	FdwX             DWORD
	FdwY             DWORD
	FdwXSize         DWORD
	FdwYSize         DWORD
	FdwXCountChars   DWORD
	FdwYCountChars   DWORD
	FdwFillAttribute DWORD
	FdwFlags         DWORD
	FwShowWindow     WORD
	FcbReserved2     WORD
	FlpReserved2     LPBYTE
	FhStdInput       HANDLE
	FhStdOutput      HANDLE
	FhStdError       HANDLE
}

type PROCESS_INFORMATION = struct {
	FhProcess    HANDLE
	FhThread     HANDLE
	FdwProcessId DWORD
	FdwThreadId  DWORD
}

/* Static assertion.  Requires support in the compiler.  */

/**
 * This file has no copyright assigned and is placed in the Public Domain.
 * This file is part of the mingw-w64 runtime package.
 * No warranty is given; refer to the file DISCLAIMER.PD within this package.
 */

/* The suffix to append to the child command lines, if any */

/* The directory separator character(s) */

/* Mark a parameter as unused to suppress compiler warnings */

// C documentation
//
//	/* Global data
//	*/
type Global = struct {
	Fargv0            uintptr
	FzVfs             uintptr
	FzDbFile          uintptr
	Fdb               uintptr
	FzErrLog          uintptr
	FpErrLog          uintptr
	FzLog             uintptr
	FpLog             uintptr
	FzName            [32]int8
	FtaskId           int32
	FiTrace           int32
	FbSqlTrace        int32
	FbIgnoreSqlErrors int32
	FnError           int32
	FnTest            int32
	FiTimeout         int32
	FbSync            int32
}

/* Static assertion.  Requires support in the compiler.  */

/**
 * This file has no copyright assigned and is placed in the Public Domain.
 * This file is part of the mingw-w64 runtime package.
 * No warranty is given; refer to the file DISCLAIMER.PD within this package.
 */

/* The suffix to append to the child command lines, if any */

/* The directory separator character(s) */

/* Mark a parameter as unused to suppress compiler warnings */

// C documentation
//
//	/* Global data
//	*/
var g Global

/* Default timeout */

// C documentation
//
//	/*
//	** Print a message adding zPrefix[] to the beginning of every line.
//	*/
func printWithPrefix(tls *libc.TLS, pOut uintptr, zPrefix uintptr, zMsg uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var __local_argv __builtin_va_list
	var __retval, i int32
	var v2 uintptr
	_, _, _, _ = __local_argv, __retval, i, v2
	for zMsg != 0 && *(*int8)(unsafe.Pointer(zMsg)) != 0 {
		i = 0
		for {
			if !(*(*int8)(unsafe.Pointer(zMsg + uintptr(i))) != 0 && int32(*(*int8)(unsafe.Pointer(zMsg + uintptr(i)))) != int32('\n') && int32(*(*int8)(unsafe.Pointer(zMsg + uintptr(i)))) != int32('\r')) {
				break
			}
			goto _1
		_1:
			;
			i++
		}
		v2 = __ccgo_ts
		libc.VaList(bp, zPrefix, i, zMsg)
		__local_argv = bp
		__retval = libc.X__mingw_vfprintf(tls, pOut, v2, __local_argv)
		_ = __local_argv
		_ = __retval
		goto _3
	_3:
		;
		zMsg += uintptr(i)
		for int32(*(*int8)(unsafe.Pointer(zMsg))) == int32('\n') || int32(*(*int8)(unsafe.Pointer(zMsg))) == int32('\r') {
			zMsg++
		}
	}
}

// C documentation
//
//	/*
//	** Compare two pointers to strings, where the pointers might be NULL.
//	*/
func safe_strcmp(tls *libc.TLS, a uintptr, b uintptr) (r int32) {
	if a == b {
		return 0
	}
	if a == uintptr(0) {
		return -int32(1)
	}
	if b == uintptr(0) {
		return int32(1)
	}
	return libc.Xstrcmp(tls, a, b)
}

// C documentation
//
//	/*
//	** Return TRUE if string z[] matches glob pattern zGlob[].
//	** Return FALSE if the pattern does not match.
//	**
//	** Globbing rules:
//	**
//	**      '*'       Matches any sequence of zero or more characters.
//	**
//	**      '?'       Matches exactly one character.
//	**
//	**     [...]      Matches one character from the enclosed list of
//	**                characters.
//	**
//	**     [^...]     Matches one character not in the enclosed list.
//	**
//	**      '#'       Matches any sequence of one or more digits with an
//	**                optional + or - sign in front
//	*/
func strglob(tls *libc.TLS, zGlob uintptr, z uintptr) (r int32) {
	var c, c2, invert, prior_c, seen, v1, v3, v7 int32
	var v10, v11, v12, v13, v14, v15, v16, v17, v2, v4, v5, v8, v9 uintptr
	var v6 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, c2, invert, prior_c, seen, v1, v10, v11, v12, v13, v14, v15, v16, v17, v2, v3, v4, v5, v6, v7, v8, v9
	for {
		v2 = zGlob
		zGlob++
		v1 = int32(*(*int8)(unsafe.Pointer(v2)))
		c = v1
		if !(v1 != 0) {
			break
		}
		if c == int32('*') {
			for {
				v4 = zGlob
				zGlob++
				v3 = int32(*(*int8)(unsafe.Pointer(v4)))
				c = v3
				if !(v3 == int32('*') || c == int32('?')) {
					break
				}
				if v6 = c == int32('?'); v6 {
					v5 = z
					z++
				}
				if v6 && int32(*(*int8)(unsafe.Pointer(v5))) == 0 {
					return 0
				}
			}
			if c == 0 {
				return int32(1)
			} else {
				if c == int32('[') {
					for *(*int8)(unsafe.Pointer(z)) != 0 && strglob(tls, zGlob-uintptr(1), z) != 0 {
						z++
					}
					return libc.BoolInt32(int32(*(*int8)(unsafe.Pointer(z))) != 0)
				}
			}
			for {
				v8 = z
				z++
				v7 = int32(*(*int8)(unsafe.Pointer(v8)))
				c2 = v7
				if !(v7 != 0) {
					break
				}
				for c2 != c {
					v9 = z
					z++
					c2 = int32(*(*int8)(unsafe.Pointer(v9)))
					if c2 == 0 {
						return 0
					}
				}
				if strglob(tls, zGlob, z) != 0 {
					return int32(1)
				}
			}
			return 0
		} else {
			if c == int32('?') {
				v10 = z
				z++
				if int32(*(*int8)(unsafe.Pointer(v10))) == 0 {
					return 0
				}
			} else {
				if c == int32('[') {
					prior_c = 0
					seen = 0
					invert = 0
					v11 = z
					z++
					c = int32(*(*int8)(unsafe.Pointer(v11)))
					if c == 0 {
						return 0
					}
					v12 = zGlob
					zGlob++
					c2 = int32(*(*int8)(unsafe.Pointer(v12)))
					if c2 == int32('^') {
						invert = int32(1)
						v13 = zGlob
						zGlob++
						c2 = int32(*(*int8)(unsafe.Pointer(v13)))
					}
					if c2 == int32(']') {
						if c == int32(']') {
							seen = int32(1)
						}
						v14 = zGlob
						zGlob++
						c2 = int32(*(*int8)(unsafe.Pointer(v14)))
					}
					for c2 != 0 && c2 != int32(']') {
						if c2 == int32('-') && int32(*(*int8)(unsafe.Pointer(zGlob))) != int32(']') && int32(*(*int8)(unsafe.Pointer(zGlob))) != 0 && prior_c > 0 {
							v15 = zGlob
							zGlob++
							c2 = int32(*(*int8)(unsafe.Pointer(v15)))
							if c >= prior_c && c <= c2 {
								seen = int32(1)
							}
							prior_c = 0
						} else {
							if c == c2 {
								seen = int32(1)
							}
							prior_c = c2
						}
						v16 = zGlob
						zGlob++
						c2 = int32(*(*int8)(unsafe.Pointer(v16)))
					}
					if c2 == 0 || seen^invert == 0 {
						return 0
					}
				} else {
					if c == int32('#') {
						if (int32(*(*int8)(unsafe.Pointer(z))) == int32('-') || int32(*(*int8)(unsafe.Pointer(z))) == int32('+')) && libc.Xisdigit(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + 1))))) != 0 {
							z++
						}
						if !(libc.Xisdigit(tls, int32(uint8(*(*int8)(unsafe.Pointer(z))))) != 0) {
							return 0
						}
						z++
						for libc.Xisdigit(tls, int32(uint8(*(*int8)(unsafe.Pointer(z))))) != 0 {
							z++
						}
					} else {
						v17 = z
						z++
						if c != int32(*(*int8)(unsafe.Pointer(v17))) {
							return 0
						}
					}
				}
			}
		}
	}
	return libc.BoolInt32(int32(*(*int8)(unsafe.Pointer(z))) == 0)
}

// C documentation
//
//	/*
//	** Close output stream pOut if it is not stdout or stderr
//	*/
func maybeClose(tls *libc.TLS, pOut uintptr) {
	if pOut != libc.X__acrt_iob_func(tls, uint32(1)) && pOut != libc.X__acrt_iob_func(tls, uint32(2)) {
		libc.Xfclose(tls, pOut)
	}
}

// C documentation
//
//	/*
//	** Print an error message
//	*/
func errorMessage(tls *libc.TLS, zFormat uintptr, va uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var ap va_list
	var zMsg uintptr
	var _ /* zPrefix at bp+0 */ [30]int8
	_, _ = ap, zMsg
	ap = va
	zMsg = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	libsqlite3.Xsqlite3_snprintf(tls, int32(30), bp, __ccgo_ts+8, libc.VaList(bp+40, uintptr(unsafe.Pointer(&g))+64))
	if g.FpLog != 0 {
		printWithPrefix(tls, g.FpLog, bp, zMsg)
		libc.Xfflush(tls, g.FpLog)
	}
	if g.FpErrLog != 0 && safe_strcmp(tls, g.FzErrLog, g.FzLog) != 0 {
		printWithPrefix(tls, g.FpErrLog, bp, zMsg)
		libc.Xfflush(tls, g.FpErrLog)
	}
	libsqlite3.Xsqlite3_free(tls, zMsg)
	g.FnError++
}

// C documentation
//
//	/*
//	** Print an error message and then quit.
//	*/
func fatalError(tls *libc.TLS, zFormat uintptr, va uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var ap va_list
	var nTry, v1 int32
	var zMsg uintptr
	var v2 bool
	var _ /* zPrefix at bp+0 */ [30]int8
	_, _, _, _, _ = ap, nTry, zMsg, v1, v2
	ap = va
	zMsg = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	libsqlite3.Xsqlite3_snprintf(tls, int32(30), bp, __ccgo_ts+19, libc.VaList(bp+40, uintptr(unsafe.Pointer(&g))+64))
	if g.FpLog != 0 {
		printWithPrefix(tls, g.FpLog, bp, zMsg)
		libc.Xfflush(tls, g.FpLog)
		maybeClose(tls, g.FpLog)
	}
	if g.FpErrLog != 0 && safe_strcmp(tls, g.FzErrLog, g.FzLog) != 0 {
		printWithPrefix(tls, g.FpErrLog, bp, zMsg)
		libc.Xfflush(tls, g.FpErrLog)
		maybeClose(tls, g.FpErrLog)
	}
	libsqlite3.Xsqlite3_free(tls, zMsg)
	if g.Fdb != 0 {
		nTry = 0
		g.FiTimeout = 0
		for {
			if v2 = trySql(tls, __ccgo_ts+30, 0) == int32(SQLITE_BUSY); v2 {
				v1 = nTry
				nTry++
			}
			if !(v2 && v1 < int32(100)) {
				break
			}
			libsqlite3.Xsqlite3_sleep(tls, int32(10))
		}
	}
	libsqlite3.Xsqlite3_close(tls, g.Fdb)
	libc.Xexit(tls, int32(1))
}

// C documentation
//
//	/*
//	** Print a log message
//	*/
func logMessage(tls *libc.TLS, zFormat uintptr, va uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var ap va_list
	var zMsg uintptr
	var _ /* zPrefix at bp+0 */ [30]int8
	_, _ = ap, zMsg
	ap = va
	zMsg = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	libsqlite3.Xsqlite3_snprintf(tls, int32(30), bp, __ccgo_ts+60, libc.VaList(bp+40, uintptr(unsafe.Pointer(&g))+64))
	if g.FpLog != 0 {
		printWithPrefix(tls, g.FpLog, bp, zMsg)
		libc.Xfflush(tls, g.FpLog)
	}
	libsqlite3.Xsqlite3_free(tls, zMsg)
}

// C documentation
//
//	/*
//	** Return the length of a string omitting trailing whitespace
//	*/
func clipLength(tls *libc.TLS, z uintptr) (r int32) {
	var n int32
	_ = n
	n = int32(libc.Xstrlen(tls, z))
	for n > 0 && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + uintptr(n-int32(1))))))) != 0 {
		n--
	}
	return n
}

// C documentation
//
//	/*
//	** Auxiliary SQL function to return the name of the VFS
//	*/
func vfsNameFunc(tls *libc.TLS, context uintptr, argc int32, argv uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var db uintptr
	var _ /* zVfs at bp+0 */ uintptr
	_ = db
	db = libsqlite3.Xsqlite3_context_db_handle(tls, context)
	*(*uintptr)(unsafe.Pointer(bp)) = uintptr(0)
	_ = argc
	_ = argv
	libsqlite3.Xsqlite3_file_control(tls, db, __ccgo_ts+65, int32(SQLITE_FCNTL_VFSNAME), bp)
	if *(*uintptr)(unsafe.Pointer(bp)) != 0 {
		libsqlite3.Xsqlite3_result_text(tls, context, *(*uintptr)(unsafe.Pointer(bp)), -int32(1), __ccgo_fp(libsqlite3.Xsqlite3_free))
	}
}

// C documentation
//
//	/*
//	** Busy handler with a g.iTimeout-millisecond timeout
//	*/
func busyHandler(tls *libc.TLS, pCD uintptr, count int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	_ = pCD
	if count*int32(10) > g.FiTimeout {
		if g.FiTimeout > 0 {
			errorMessage(tls, __ccgo_ts+70, libc.VaList(bp+8, g.FiTimeout))
		}
		return 0
	}
	libsqlite3.Xsqlite3_sleep(tls, int32(10))
	return int32(1)
}

// C documentation
//
//	/*
//	** SQL Trace callback
//	*/
func sqlTraceCallback(tls *libc.TLS, NotUsed1 uintptr, zSql uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	_ = NotUsed1
	logMessage(tls, __ccgo_ts+89, libc.VaList(bp+8, clipLength(tls, zSql), zSql))
}

// C documentation
//
//	/*
//	** SQL error log callback
//	*/
func sqlErrorCallback(tls *libc.TLS, pArg uintptr, iErrCode int32, zMsg uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	_ = pArg
	if iErrCode == int32(SQLITE_ERROR) && g.FbIgnoreSqlErrors != 0 {
		return
	}
	if iErrCode&int32(0xff) == int32(SQLITE_SCHEMA) && g.FiTrace < int32(3) {
		return
	}
	if g.FiTimeout == 0 && iErrCode&int32(0xff) == int32(SQLITE_BUSY) && g.FiTrace < int32(3) {
		return
	}
	if iErrCode&int32(0xff) == int32(SQLITE_NOTICE) {
		logMessage(tls, __ccgo_ts+96, libc.VaList(bp+8, zMsg))
	} else {
		errorMessage(tls, __ccgo_ts+106, libc.VaList(bp+8, iErrCode, zMsg))
	}
}

// C documentation
//
//	/*
//	** Prepare an SQL statement.  Issue a fatal error if unable.
//	*/
func prepareSql(tls *libc.TLS, zFormat uintptr, va uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var ap va_list
	var rc int32
	var zSql uintptr
	var _ /* pStmt at bp+0 */ uintptr
	_, _, _ = ap, rc, zSql
	*(*uintptr)(unsafe.Pointer(bp)) = uintptr(0)
	ap = va
	zSql = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	rc = libsqlite3.Xsqlite3_prepare_v2(tls, g.Fdb, zSql, -int32(1), bp, uintptr(0))
	if rc != SQLITE_OK {
		libsqlite3.Xsqlite3_finalize(tls, *(*uintptr)(unsafe.Pointer(bp)))
		fatalError(tls, __ccgo_ts+122, libc.VaList(bp+16, libsqlite3.Xsqlite3_errmsg(tls, g.Fdb), zSql))
	}
	libsqlite3.Xsqlite3_free(tls, zSql)
	return *(*uintptr)(unsafe.Pointer(bp))
}

// C documentation
//
//	/*
//	** Run arbitrary SQL.  Issue a fatal error on failure.
//	*/
func runSql(tls *libc.TLS, zFormat uintptr, va uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var ap va_list
	var rc int32
	var zSql uintptr
	_, _, _ = ap, rc, zSql
	ap = va
	zSql = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	rc = libsqlite3.Xsqlite3_exec(tls, g.Fdb, zSql, uintptr(0), uintptr(0), uintptr(0))
	if rc != SQLITE_OK {
		fatalError(tls, __ccgo_ts+122, libc.VaList(bp+8, libsqlite3.Xsqlite3_errmsg(tls, g.Fdb), zSql))
	}
	libsqlite3.Xsqlite3_free(tls, zSql)
}

// C documentation
//
//	/*
//	** Try to run arbitrary SQL.  Return success code.
//	*/
func trySql(tls *libc.TLS, zFormat uintptr, va uintptr) (r int32) {
	var ap va_list
	var rc int32
	var zSql uintptr
	_, _, _ = ap, rc, zSql
	ap = va
	zSql = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	rc = libsqlite3.Xsqlite3_exec(tls, g.Fdb, zSql, uintptr(0), uintptr(0), uintptr(0))
	libsqlite3.Xsqlite3_free(tls, zSql)
	return rc
}

// C documentation
//
//	/* Structure for holding an arbitrary length string
//	*/
type String = struct {
	Fz      uintptr
	Fn      int32
	FnAlloc int32
}

// C documentation
//
//	/* Free a string */
func stringFree(tls *libc.TLS, p uintptr) {
	if (*String)(unsafe.Pointer(p)).Fz != 0 {
		libsqlite3.Xsqlite3_free(tls, (*String)(unsafe.Pointer(p)).Fz)
	}
	libc.Xmemset(tls, p, 0, uint64(16))
}

// C documentation
//
//	/* Append n bytes of text to a string.  If n<0 append the entire string. */
func stringAppend(tls *libc.TLS, p uintptr, z uintptr, n int32) {
	var nAlloc int32
	var zNew uintptr
	_, _ = nAlloc, zNew
	if n < 0 {
		n = int32(libc.Xstrlen(tls, z))
	}
	if (*String)(unsafe.Pointer(p)).Fn+n >= (*String)(unsafe.Pointer(p)).FnAlloc {
		nAlloc = (*String)(unsafe.Pointer(p)).FnAlloc*int32(2) + n + int32(100)
		zNew = libsqlite3.Xsqlite3_realloc(tls, (*String)(unsafe.Pointer(p)).Fz, nAlloc)
		if zNew == uintptr(0) {
			fatalError(tls, __ccgo_ts+129, 0)
		}
		(*String)(unsafe.Pointer(p)).Fz = zNew
		(*String)(unsafe.Pointer(p)).FnAlloc = nAlloc
	}
	libc.Xmemcpy(tls, (*String)(unsafe.Pointer(p)).Fz+uintptr((*String)(unsafe.Pointer(p)).Fn), z, uint64(n))
	*(*int32)(unsafe.Pointer(p + 8)) += n
	*(*int8)(unsafe.Pointer((*String)(unsafe.Pointer(p)).Fz + uintptr((*String)(unsafe.Pointer(p)).Fn))) = 0
}

// C documentation
//
//	/* Reset a string to an empty string */
func stringReset(tls *libc.TLS, p uintptr) {
	if (*String)(unsafe.Pointer(p)).Fz == uintptr(0) {
		stringAppend(tls, p, __ccgo_ts+143, int32(1))
	}
	(*String)(unsafe.Pointer(p)).Fn = 0
	*(*int8)(unsafe.Pointer((*String)(unsafe.Pointer(p)).Fz)) = 0
}

// C documentation
//
//	/* Append a new token onto the end of the string */
func stringAppendTerm(tls *libc.TLS, p uintptr, z uintptr) {
	var i int32
	_ = i
	if (*String)(unsafe.Pointer(p)).Fn != 0 {
		stringAppend(tls, p, __ccgo_ts+143, int32(1))
	}
	if z == uintptr(0) {
		stringAppend(tls, p, __ccgo_ts+145, int32(3))
		return
	}
	i = 0
	for {
		if !(*(*int8)(unsafe.Pointer(z + uintptr(i))) != 0 && !(libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + uintptr(i)))))) != 0)) {
			break
		}
		goto _1
	_1:
		;
		i++
	}
	if i > 0 && int32(*(*int8)(unsafe.Pointer(z + uintptr(i)))) == 0 {
		stringAppend(tls, p, z, i)
		return
	}
	stringAppend(tls, p, __ccgo_ts+149, int32(1))
	for *(*int8)(unsafe.Pointer(z)) != 0 {
		i = 0
		for {
			if !(*(*int8)(unsafe.Pointer(z + uintptr(i))) != 0 && int32(*(*int8)(unsafe.Pointer(z + uintptr(i)))) != int32('\'')) {
				break
			}
			goto _2
		_2:
			;
			i++
		}
		if *(*int8)(unsafe.Pointer(z + uintptr(i))) != 0 {
			stringAppend(tls, p, z, i+int32(1))
			stringAppend(tls, p, __ccgo_ts+149, int32(1))
			z += uintptr(i + int32(1))
		} else {
			stringAppend(tls, p, z, i)
			break
		}
	}
	stringAppend(tls, p, __ccgo_ts+149, int32(1))
}

// C documentation
//
//	/*
//	** Callback function for evalSql()
//	*/
func evalCallback(tls *libc.TLS, pCData uintptr, argc int32, argv uintptr, azCol uintptr) (r int32) {
	var i int32
	var p uintptr
	_, _ = i, p
	p = pCData
	_ = azCol
	i = 0
	for {
		if !(i < argc) {
			break
		}
		stringAppendTerm(tls, p, *(*uintptr)(unsafe.Pointer(argv + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	return 0
}

// C documentation
//
//	/*
//	** Run arbitrary SQL and record the results in an output string
//	** given by the first parameter.
//	*/
func evalSql(tls *libc.TLS, p uintptr, zFormat uintptr, va uintptr) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var ap va_list
	var rc int32
	var zSql uintptr
	var _ /* zErr at bp+8 */ [30]int8
	var _ /* zErrMsg at bp+0 */ uintptr
	_, _, _ = ap, rc, zSql
	*(*uintptr)(unsafe.Pointer(bp)) = uintptr(0)
	ap = va
	zSql = libsqlite3.Xsqlite3_vmprintf(tls, zFormat, ap)
	_ = ap
	rc = libsqlite3.Xsqlite3_exec(tls, g.Fdb, zSql, __ccgo_fp(evalCallback), p, bp)
	libsqlite3.Xsqlite3_free(tls, zSql)
	if rc != 0 {
		libsqlite3.Xsqlite3_snprintf(tls, int32(30), bp+8, __ccgo_ts+151, libc.VaList(bp+48, rc))
		stringAppendTerm(tls, p, bp+8)
		if *(*uintptr)(unsafe.Pointer(bp)) != 0 {
			stringAppendTerm(tls, p, *(*uintptr)(unsafe.Pointer(bp)))
			libsqlite3.Xsqlite3_free(tls, *(*uintptr)(unsafe.Pointer(bp)))
		}
	}
	return rc
}

// C documentation
//
//	/*
//	** Auxiliary SQL function to recursively evaluate SQL.
//	*/
func evalFunc(tls *libc.TLS, context uintptr, argc int32, argv uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var db, zSql uintptr
	var rc int32
	var _ /* res at bp+0 */ String
	var _ /* zErrMsg at bp+16 */ uintptr
	_, _, _ = db, rc, zSql
	db = libsqlite3.Xsqlite3_context_db_handle(tls, context)
	zSql = libsqlite3.Xsqlite3_value_text(tls, *(*uintptr)(unsafe.Pointer(argv)))
	*(*uintptr)(unsafe.Pointer(bp + 16)) = uintptr(0)
	_ = argc
	libc.Xmemset(tls, bp, 0, uint64(16))
	rc = libsqlite3.Xsqlite3_exec(tls, db, zSql, __ccgo_fp(evalCallback), bp, bp+16)
	if *(*uintptr)(unsafe.Pointer(bp + 16)) != 0 {
		libsqlite3.Xsqlite3_result_error(tls, context, *(*uintptr)(unsafe.Pointer(bp + 16)), -int32(1))
		libsqlite3.Xsqlite3_free(tls, *(*uintptr)(unsafe.Pointer(bp + 16)))
	} else {
		if rc != 0 {
			libsqlite3.Xsqlite3_result_error_code(tls, context, rc)
		} else {
			libsqlite3.Xsqlite3_result_text(tls, context, (*(*String)(unsafe.Pointer(bp))).Fz, -int32(1), uintptr(-libc.Int32FromInt32(1)))
		}
	}
	stringFree(tls, bp)
}

// C documentation
//
//	/*
//	** Look up the next task for client iClient in the database.
//	** Return the task script and the task number and mark that
//	** task as being under way.
//	*/
func startScript(tls *libc.TLS, iClient int32, pzScript uintptr, pTaskId uintptr, pzTaskName uintptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var n, rc, taskId, totalTime, v1 int32
	var pStmt uintptr
	_, _, _, _, _, _ = n, pStmt, rc, taskId, totalTime, v1
	pStmt = uintptr(0)
	totalTime = 0
	*(*uintptr)(unsafe.Pointer(pzScript)) = uintptr(0)
	g.FiTimeout = 0
	for int32(1) != 0 {
		rc = trySql(tls, __ccgo_ts+161, 0)
		if rc == int32(SQLITE_BUSY) {
			libsqlite3.Xsqlite3_sleep(tls, int32(10))
			totalTime += int32(10)
			continue
		}
		if rc != SQLITE_OK {
			fatalError(tls, __ccgo_ts+177, libc.VaList(bp+8, libsqlite3.Xsqlite3_errmsg(tls, g.Fdb)))
		}
		if g.FnError != 0 || g.FnTest != 0 {
			runSql(tls, __ccgo_ts+196, libc.VaList(bp+8, g.FnError, g.FnTest))
			g.FnError = 0
			g.FnTest = 0
		}
		pStmt = prepareSql(tls, __ccgo_ts+249, libc.VaList(bp+8, iClient))
		rc = libsqlite3.Xsqlite3_step(tls, pStmt)
		libsqlite3.Xsqlite3_finalize(tls, pStmt)
		if rc == int32(SQLITE_ROW) {
			runSql(tls, __ccgo_ts+295, libc.VaList(bp+8, iClient))
			g.FiTimeout = int32(DEFAULT_TIMEOUT)
			runSql(tls, __ccgo_ts+326, 0)
			return int32(SQLITE_DONE)
		}
		pStmt = prepareSql(tls, __ccgo_ts+346, libc.VaList(bp+8, iClient))
		rc = libsqlite3.Xsqlite3_step(tls, pStmt)
		if rc == int32(SQLITE_ROW) {
			n = libsqlite3.Xsqlite3_column_bytes(tls, pStmt, 0)
			*(*uintptr)(unsafe.Pointer(pzScript)) = libsqlite3.Xsqlite3_malloc(tls, n+int32(1))
			libc.Xstrcpy(tls, *(*uintptr)(unsafe.Pointer(pzScript)), libsqlite3.Xsqlite3_column_text(tls, pStmt, 0))
			v1 = libsqlite3.Xsqlite3_column_int(tls, pStmt, int32(1))
			taskId = v1
			*(*int32)(unsafe.Pointer(pTaskId)) = v1
			*(*uintptr)(unsafe.Pointer(pzTaskName)) = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+438, libc.VaList(bp+8, libsqlite3.Xsqlite3_column_text(tls, pStmt, int32(2))))
			libsqlite3.Xsqlite3_finalize(tls, pStmt)
			runSql(tls, __ccgo_ts+441, libc.VaList(bp+8, taskId))
			g.FiTimeout = int32(DEFAULT_TIMEOUT)
			runSql(tls, __ccgo_ts+326, 0)
			return SQLITE_OK
		}
		libsqlite3.Xsqlite3_finalize(tls, pStmt)
		if rc == int32(SQLITE_DONE) {
			if totalTime > int32(30000) {
				errorMessage(tls, __ccgo_ts+524, 0)
				runSql(tls, __ccgo_ts+573, libc.VaList(bp+8, iClient))
				libsqlite3.Xsqlite3_close(tls, g.Fdb)
				libc.Xexit(tls, int32(1))
			}
			for trySql(tls, __ccgo_ts+613, 0) == int32(SQLITE_BUSY) {
				libsqlite3.Xsqlite3_sleep(tls, int32(10))
				totalTime += int32(10)
			}
			libsqlite3.Xsqlite3_sleep(tls, int32(100))
			totalTime += int32(100)
			continue
		}
		fatalError(tls, __ccgo_ts+438, libc.VaList(bp+8, libsqlite3.Xsqlite3_errmsg(tls, g.Fdb)))
	}
	g.FiTimeout = int32(DEFAULT_TIMEOUT)
	return r
}

// C documentation
//
//	/*
//	** Mark a script as having finished.   Remove the CLIENT table entry
//	** if bShutdown is true.
//	*/
func finishScript(tls *libc.TLS, iClient int32, taskId int32, bShutdown int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	runSql(tls, __ccgo_ts+620, libc.VaList(bp+8, taskId))
	if bShutdown != 0 {
		runSql(tls, __ccgo_ts+295, libc.VaList(bp+8, iClient))
	}
	return SQLITE_OK
}

// C documentation
//
//	/*
//	** Start up a client process for iClient, if it is not already
//	** running.  If the client is already running, then this routine
//	** is a no-op.
//	*/
func startClient(tls *libc.TLS, iClient int32) {
	bp := tls.Alloc(176)
	defer tls.Free(176)
	var rc int32
	var zSys uintptr
	var _ /* processInfo at bp+104 */ PROCESS_INFORMATION
	var _ /* startupInfo at bp+0 */ STARTUPINFOA
	_, _ = rc, zSys
	runSql(tls, __ccgo_ts+701, libc.VaList(bp+136, iClient))
	if libsqlite3.Xsqlite3_changes(tls, g.Fdb) != 0 {
		zSys = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+743, libc.VaList(bp+136, g.Fargv0, g.FzDbFile, iClient, g.FiTrace))
		if g.FbSqlTrace != 0 {
			zSys = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+774, libc.VaList(bp+136, zSys))
		}
		if g.FbSync != 0 {
			zSys = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+788, libc.VaList(bp+136, zSys))
		}
		if g.FzVfs != 0 {
			zSys = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+798, libc.VaList(bp+136, zSys, g.FzVfs))
		}
		if g.FiTrace >= int32(2) {
			logMessage(tls, __ccgo_ts+812, libc.VaList(bp+136, zSys))
		}
		libc.Xmemset(tls, bp, 0, uint64(104))
		(*(*STARTUPINFOA)(unsafe.Pointer(bp))).Fcb = uint32(104)
		libc.Xmemset(tls, bp+104, 0, uint64(24))
		rc = libc.XCreateProcessA(tls, libc.UintptrFromInt32(0), zSys, libc.UintptrFromInt32(0), libc.UintptrFromInt32(0), FALSE, uint32(0), libc.UintptrFromInt32(0), libc.UintptrFromInt32(0), bp, bp+104)
		if rc != 0 {
			libc.XCloseHandle(tls, (*(*PROCESS_INFORMATION)(unsafe.Pointer(bp + 104))).FhThread)
			libc.XCloseHandle(tls, (*(*PROCESS_INFORMATION)(unsafe.Pointer(bp + 104))).FhProcess)
		} else {
			errorMessage(tls, __ccgo_ts+825, libc.VaList(bp+136, libc.XGetLastError(tls)))
		}
		libsqlite3.Xsqlite3_free(tls, zSys)
	}
}

// C documentation
//
//	/*
//	** Read the entire content of a file into memory
//	*/
func readFile(tls *libc.TLS, zFilename uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var in, z uintptr
	var sz int32
	_, _, _ = in, sz, z
	in = libc.Xfopen(tls, zFilename, __ccgo_ts+868)
	if in == uintptr(0) {
		fatalError(tls, __ccgo_ts+871, libc.VaList(bp+8, zFilename))
	}
	libc.Xfseek(tls, in, 0, int32(SEEK_END))
	sz = libc.Xftell(tls, in)
	libc.Xrewind(tls, in)
	z = libsqlite3.Xsqlite3_malloc(tls, sz+int32(1))
	sz = int32(libc.Xfread(tls, z, uint64(1), uint64(sz), in))
	*(*int8)(unsafe.Pointer(z + uintptr(sz))) = 0
	libc.Xfclose(tls, in)
	return z
}

// C documentation
//
//	/*
//	** Return the length of the next token.
//	*/
func tokenLength(tls *libc.TLS, z uintptr, pnLine uintptr) (r int32) {
	var c, c1, delim, inC, n, v1, v2, v6 int32
	_, _, _, _, _, _, _, _ = c, c1, delim, inC, n, v1, v2, v6
	n = 0
	if libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z))))) != 0 || int32(*(*int8)(unsafe.Pointer(z))) == int32('/') && int32(*(*int8)(unsafe.Pointer(z + 1))) == int32('*') {
		inC = 0
		if int32(*(*int8)(unsafe.Pointer(z))) == int32('/') {
			inC = int32(1)
			n = int32(2)
		}
		for {
			v2 = n
			n++
			v1 = int32(*(*int8)(unsafe.Pointer(z + uintptr(v2))))
			c = v1
			if !(v1 != 0) {
				break
			}
			if c == int32('\n') {
				*(*int32)(unsafe.Pointer(pnLine))++
			}
			if libc.Xisspace(tls, int32(uint8(c))) != 0 {
				continue
			}
			if inC != 0 && c == int32('*') && int32(*(*int8)(unsafe.Pointer(z + uintptr(n)))) == int32('/') {
				n++
				inC = 0
			} else {
				if !(inC != 0) && c == int32('/') && int32(*(*int8)(unsafe.Pointer(z + uintptr(n)))) == int32('*') {
					n++
					inC = int32(1)
				} else {
					if !(inC != 0) {
						break
					}
				}
			}
		}
		n--
	} else {
		if int32(*(*int8)(unsafe.Pointer(z))) == int32('-') && int32(*(*int8)(unsafe.Pointer(z + 1))) == int32('-') {
			n = int32(2)
			for {
				if !(*(*int8)(unsafe.Pointer(z + uintptr(n))) != 0 && int32(*(*int8)(unsafe.Pointer(z + uintptr(n)))) != int32('\n')) {
					break
				}
				goto _3
			_3:
				;
				n++
			}
			if *(*int8)(unsafe.Pointer(z + uintptr(n))) != 0 {
				*(*int32)(unsafe.Pointer(pnLine))++
				n++
			}
		} else {
			if int32(*(*int8)(unsafe.Pointer(z))) == int32('"') || int32(*(*int8)(unsafe.Pointer(z))) == int32('\'') {
				delim = int32(*(*int8)(unsafe.Pointer(z)))
				n = int32(1)
				for {
					if !(*(*int8)(unsafe.Pointer(z + uintptr(n))) != 0) {
						break
					}
					if int32(*(*int8)(unsafe.Pointer(z + uintptr(n)))) == int32('\n') {
						*(*int32)(unsafe.Pointer(pnLine))++
					}
					if int32(*(*int8)(unsafe.Pointer(z + uintptr(n)))) == delim {
						n++
						if int32(*(*int8)(unsafe.Pointer(z + uintptr(n+int32(1))))) != delim {
							break
						}
					}
					goto _4
				_4:
					;
					n++
				}
			} else {
				n = int32(1)
				for {
					v6 = int32(*(*int8)(unsafe.Pointer(z + uintptr(n))))
					c1 = v6
					if !(v6 != 0 && !(libc.Xisspace(tls, int32(uint8(c1))) != 0) && c1 != int32('"') && c1 != int32('\'') && c1 != int32(';')) {
						break
					}
					goto _5
				_5:
					;
					n++
				}
			}
		}
	}
	return n
}

// C documentation
//
//	/*
//	** Copy a single token into a string buffer.
//	*/
func extractToken(tls *libc.TLS, zIn uintptr, nIn int32, zOut uintptr, nOut int32) (r int32) {
	var i int32
	_ = i
	if nIn <= 0 {
		*(*int8)(unsafe.Pointer(zOut)) = 0
		return 0
	}
	i = 0
	for {
		if !(i < nIn && i < nOut-int32(1) && !(libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(zIn + uintptr(i)))))) != 0)) {
			break
		}
		*(*int8)(unsafe.Pointer(zOut + uintptr(i))) = *(*int8)(unsafe.Pointer(zIn + uintptr(i)))
		goto _1
	_1:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(zOut + uintptr(i))) = 0
	return i
}

// C documentation
//
//	/*
//	** Find the number of characters up to the start of the next "--end" token.
//	*/
func findEnd(tls *libc.TLS, z uintptr, pnLine uintptr) (r int32) {
	var n int32
	_ = n
	n = 0
	for *(*int8)(unsafe.Pointer(z + uintptr(n))) != 0 && (libc.Xstrncmp(tls, z+uintptr(n), __ccgo_ts+900, uint64(5)) != 0 || !(libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + uintptr(n+int32(5))))))) != 0)) {
		n += tokenLength(tls, z+uintptr(n), pnLine)
	}
	return n
}

// C documentation
//
//	/*
//	** Find the number of characters up to the first character past the
//	** of the next "--endif"  or "--else" token. Nested --if commands are
//	** also skipped.
//	*/
func findEndif(tls *libc.TLS, z uintptr, stopAtElse int32, pnLine uintptr) (r int32) {
	var len1, n, skip int32
	_, _, _ = len1, n, skip
	n = 0
	for *(*int8)(unsafe.Pointer(z + uintptr(n))) != 0 {
		len1 = tokenLength(tls, z+uintptr(n), pnLine)
		if libc.Xstrncmp(tls, z+uintptr(n), __ccgo_ts+906, uint64(7)) == 0 && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + uintptr(n+int32(7))))))) != 0 || stopAtElse != 0 && libc.Xstrncmp(tls, z+uintptr(n), __ccgo_ts+914, uint64(6)) == 0 && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + uintptr(n+int32(6))))))) != 0 {
			return n + len1
		}
		if libc.Xstrncmp(tls, z+uintptr(n), __ccgo_ts+921, uint64(4)) == 0 && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(z + uintptr(n+int32(4))))))) != 0 {
			skip = findEndif(tls, z+uintptr(n)+uintptr(len1), 0, pnLine)
			n += skip + len1
		} else {
			n += len1
		}
	}
	return n
}

// C documentation
//
//	/*
//	** Wait for a client process to complete all its tasks
//	*/
func waitForClient(tls *libc.TLS, iClient int32, iTimeout int32, zErrPrefix uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var pStmt uintptr
	var rc, v1 int32
	_, _, _ = pStmt, rc, v1
	if iClient > 0 {
		pStmt = prepareSql(tls, __ccgo_ts+926, libc.VaList(bp+8, iClient))
	} else {
		pStmt = prepareSql(tls, __ccgo_ts+1022, 0)
	}
	g.FiTimeout = 0
	for {
		v1 = libsqlite3.Xsqlite3_step(tls, pStmt)
		rc = v1
		if !((v1 == int32(SQLITE_BUSY) || rc == int32(SQLITE_ROW)) && iTimeout > 0) {
			break
		}
		libsqlite3.Xsqlite3_reset(tls, pStmt)
		libsqlite3.Xsqlite3_sleep(tls, int32(50))
		iTimeout -= int32(50)
	}
	libsqlite3.Xsqlite3_finalize(tls, pStmt)
	g.FiTimeout = int32(DEFAULT_TIMEOUT)
	if rc != int32(SQLITE_DONE) {
		if zErrPrefix == uintptr(0) {
			zErrPrefix = __ccgo_ts + 1103
		}
		if iClient > 0 {
			errorMessage(tls, __ccgo_ts+1104, libc.VaList(bp+8, zErrPrefix, iClient))
		} else {
			errorMessage(tls, __ccgo_ts+1136, libc.VaList(bp+8, zErrPrefix))
		}
	}
}

// C documentation
//
//	/* Return a pointer to the tail of a filename
//	*/
func filenameTail(tls *libc.TLS, z uintptr) (r uintptr) {
	var i, j, v2 int32
	_, _, _ = i, j, v2
	v2 = libc.Int32FromInt32(0)
	j = v2
	i = v2
	for {
		if !(*(*int8)(unsafe.Pointer(z + uintptr(i))) != 0) {
			break
		}
		if int32(*(*int8)(unsafe.Pointer(z + uintptr(i)))) == int32('/') || int32(*(*int8)(unsafe.Pointer(z + uintptr(i)))) == int32('\\') {
			j = i + int32(1)
		}
		goto _1
	_1:
		;
		i++
	}
	return z + uintptr(j)
}

// C documentation
//
//	/*
//	** Interpret zArg as a boolean value.  Return either 0 or 1.
//	*/
func booleanValue(tls *libc.TLS, zArg uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	_ = i
	if zArg == uintptr(0) {
		return 0
	}
	i = 0
	for {
		if !(int32(*(*int8)(unsafe.Pointer(zArg + uintptr(i)))) >= int32('0') && int32(*(*int8)(unsafe.Pointer(zArg + uintptr(i)))) <= int32('9')) {
			break
		}
		goto _1
	_1:
		;
		i++
	}
	if i > 0 && int32(*(*int8)(unsafe.Pointer(zArg + uintptr(i)))) == 0 {
		return libc.Xatoi(tls, zArg)
	}
	if libsqlite3.Xsqlite3_stricmp(tls, zArg, __ccgo_ts+1170) == 0 || libsqlite3.Xsqlite3_stricmp(tls, zArg, __ccgo_ts+1173) == 0 {
		return int32(1)
	}
	if libsqlite3.Xsqlite3_stricmp(tls, zArg, __ccgo_ts+1177) == 0 || libsqlite3.Xsqlite3_stricmp(tls, zArg, __ccgo_ts+1181) == 0 {
		return 0
	}
	errorMessage(tls, __ccgo_ts+1184, libc.VaList(bp+8, zArg))
	return 0
}

// C documentation
//
//	/* This routine exists as a convenient place to set a debugger
//	** breakpoint.
//	*/
func test_breakpoint(tls *libc.TLS) {
	libc.PostIncAtomicInt32P(uintptr(unsafe.Pointer(&cnt)), 1)
}

var cnt int32

/* Maximum number of arguments to a --command */

// C documentation
//
//	/*
//	** Run a script.
//	*/
func runScript(tls *libc.TLS, iClient int32, taskId int32, zScript uintptr, zFilename uintptr) {
	bp := tls.Alloc(1312)
	defer tls.Free(1312)
	var c, iBegin, iEnd, iNewClient, iTarget, iTimeout, ii, isGlob, j, jj, jj1, jj2, jj3, k, len1, n, nArg, prevLine, rc, rc1, v1, v11, v12, v4 int32
	var pStmt, zAns, zAns1, zCopy, zNewFile, zNewScript, zSql, zSql1, zTName, zTask, zToDel, v8 uintptr
	var _ /* azArg at bp+1054 */ [2][100]int8
	var _ /* lineno at bp+0 */ int32
	var _ /* sResult at bp+8 */ String
	var _ /* zCmd at bp+24 */ [30]int8
	var _ /* zError at bp+54 */ [1000]int8
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = c, iBegin, iEnd, iNewClient, iTarget, iTimeout, ii, isGlob, j, jj, jj1, jj2, jj3, k, len1, n, nArg, pStmt, prevLine, rc, rc1, zAns, zAns1, zCopy, zNewFile, zNewScript, zSql, zSql1, zTName, zTask, zToDel, v1, v11, v12, v4, v8
	*(*int32)(unsafe.Pointer(bp)) = int32(1)
	prevLine = int32(1)
	ii = 0
	iBegin = 0
	libc.Xmemset(tls, bp+8, 0, uint64(16))
	stringReset(tls, bp+8)
	for {
		v1 = int32(*(*int8)(unsafe.Pointer(zScript + uintptr(ii))))
		c = v1
		if !(v1 != 0) {
			break
		}
		prevLine = *(*int32)(unsafe.Pointer(bp))
		len1 = tokenLength(tls, zScript+uintptr(ii), bp)
		if libc.Xisspace(tls, int32(uint8(c))) != 0 || c == int32('/') && int32(*(*int8)(unsafe.Pointer(zScript + uintptr(ii+int32(1))))) == int32('*') {
			ii += len1
			continue
		}
		if c != int32('-') || int32(*(*int8)(unsafe.Pointer(zScript + uintptr(ii+int32(1))))) != int32('-') || !(libc.Xisalpha(tls, int32(*(*int8)(unsafe.Pointer(zScript + uintptr(ii+int32(2)))))) != 0) {
			ii += len1
			continue
		}
		/* Run any prior SQL before processing the new --command */
		if ii > iBegin {
			zSql = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+1206, libc.VaList(bp+1264, ii-iBegin, zScript+uintptr(iBegin)))
			evalSql(tls, bp+8, zSql, 0)
			libsqlite3.Xsqlite3_free(tls, zSql)
			iBegin = ii + len1
		}
		/* Parse the --command */
		if g.FiTrace >= int32(2) {
			logMessage(tls, __ccgo_ts+1206, libc.VaList(bp+1264, len1, zScript+uintptr(ii)))
		}
		n = extractToken(tls, zScript+uintptr(ii)+uintptr(2), len1-int32(2), bp+24, int32(30))
		nArg = 0
		for {
			if !(n < len1-int32(2) && nArg < int32(MX_ARG)) {
				break
			}
			for n < len1-int32(2) && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(zScript + uintptr(ii+int32(2)+n)))))) != 0 {
				n++
			}
			if n >= len1-int32(2) {
				break
			}
			n += extractToken(tls, zScript+uintptr(ii)+uintptr(2)+uintptr(n), len1-int32(2)-n, bp+1054+uintptr(nArg)*100, int32(100))
			goto _2
		_2:
			;
			nArg++
		}
		j = nArg
		for {
			if !(j < int32(MX_ARG)) {
				break
			}
			v4 = j
			j++
			*(*int8)(unsafe.Pointer(bp + 1054 + uintptr(v4)*100)) = 0
			goto _3
		_3:
			;
			j++
		}
		/*
		 **  --sleep N
		 **
		 ** Pause for N milliseconds
		 */
		if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1211) == 0 {
			libsqlite3.Xsqlite3_sleep(tls, libc.Xatoi(tls, bp+1054))
		} else {
			/*
			 **   --exit N
			 **
			 ** Exit this process.  If N>0 then exit without shutting down
			 ** SQLite.  (In other words, simulate a crash.)
			 */
			if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1217) == 0 {
				rc = libc.Xatoi(tls, bp+1054)
				finishScript(tls, iClient, taskId, int32(1))
				if rc == 0 {
					libsqlite3.Xsqlite3_close(tls, g.Fdb)
				}
				libc.Xexit(tls, rc)
			} else {
				/*
				 **   --testcase NAME
				 **
				 ** Begin a new test case.  Announce in the log that the test case
				 ** has begun.
				 */
				if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1222) == 0 {
					if g.FiTrace == int32(1) {
						logMessage(tls, __ccgo_ts+1206, libc.VaList(bp+1264, len1-int32(1), zScript+uintptr(ii)))
					}
					stringReset(tls, bp+8)
				} else {
					/*
					 **   --finish
					 **
					 ** Mark the current task as having finished, even if it is not.
					 ** This can be used in conjunction with --exit to simulate a crash.
					 */
					if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1231) == 0 && iClient > 0 {
						finishScript(tls, iClient, taskId, int32(1))
					} else {
						/*
						 **  --reset
						 **
						 ** Reset accumulated results back to an empty string
						 */
						if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1238) == 0 {
							stringReset(tls, bp+8)
						} else {
							/*
							 **  --match ANSWER...
							 **
							 ** Check to see if output matches ANSWER.  Report an error if not.
							 */
							if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1244) == 0 {
								zAns = zScript + uintptr(ii)
								jj = int32(7)
								for {
									if !(jj < len1-int32(1) && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(zAns + uintptr(jj)))))) != 0) {
										break
									}
									goto _5
								_5:
									;
									jj++
								}
								zAns += uintptr(jj)
								if len1-jj-int32(1) != (*(*String)(unsafe.Pointer(bp + 8))).Fn || libc.Xstrncmp(tls, (*(*String)(unsafe.Pointer(bp + 8))).Fz, zAns, uint64(len1-jj-int32(1))) != 0 {
									errorMessage(tls, __ccgo_ts+1250, libc.VaList(bp+1264, prevLine, zFilename, len1-jj-int32(1), zAns, (*(*String)(unsafe.Pointer(bp + 8))).Fz))
								}
								g.FnTest++
								stringReset(tls, bp+8)
							} else {
								/*
								 **  --glob ANSWER...
								 **  --notglob ANSWER....
								 **
								 ** Check to see if output does or does not match the glob pattern
								 ** ANSWER.
								 */
								if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1295) == 0 || libc.Xstrcmp(tls, bp+24, __ccgo_ts+1300) == 0 {
									zAns1 = zScript + uintptr(ii)
									isGlob = libc.BoolInt32(int32((*(*[30]int8)(unsafe.Pointer(bp + 24)))[0]) == int32('g'))
									jj1 = int32(9) - int32(3)*isGlob
									for {
										if !(jj1 < len1-int32(1) && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(zAns1 + uintptr(jj1)))))) != 0) {
											break
										}
										goto _6
									_6:
										;
										jj1++
									}
									zAns1 += uintptr(jj1)
									zCopy = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+1206, libc.VaList(bp+1264, len1-jj1-int32(1), zAns1))
									if libc.BoolInt32(libsqlite3.Xsqlite3_strglob(tls, zCopy, (*(*String)(unsafe.Pointer(bp + 8))).Fz) == 0)^isGlob != 0 {
										errorMessage(tls, __ccgo_ts+1308, libc.VaList(bp+1264, prevLine, zFilename, zCopy, (*(*String)(unsafe.Pointer(bp + 8))).Fz))
									}
									libsqlite3.Xsqlite3_free(tls, zCopy)
									g.FnTest++
									stringReset(tls, bp+8)
								} else {
									/*
									 **  --output
									 **
									 ** Output the result of the previous SQL.
									 */
									if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1351) == 0 {
										logMessage(tls, __ccgo_ts+438, libc.VaList(bp+1264, (*(*String)(unsafe.Pointer(bp + 8))).Fz))
									} else {
										/*
										 **  --source FILENAME
										 **
										 ** Run a subscript from a separate file.
										 */
										if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1358) == 0 {
											zToDel = uintptr(0)
											zNewFile = bp + 1054
											if !(int32(*(*int8)(unsafe.Pointer(zNewFile))) == int32('/') || int32(*(*int8)(unsafe.Pointer(zNewFile))) == int32('\\')) {
												k = int32(libc.Xstrlen(tls, zFilename)) - int32(1)
												for {
													if !(k >= 0 && !(int32(*(*int8)(unsafe.Pointer(zFilename + uintptr(k)))) == int32('/') || int32(*(*int8)(unsafe.Pointer(zFilename + uintptr(k)))) == int32('\\'))) {
														break
													}
													goto _7
												_7:
													;
													k--
												}
												if k > 0 {
													v8 = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+1365, libc.VaList(bp+1264, k, zFilename, zNewFile))
													zToDel = v8
													zNewFile = v8
												}
											}
											zNewScript = readFile(tls, zNewFile)
											if g.FiTrace != 0 {
												logMessage(tls, __ccgo_ts+1373, libc.VaList(bp+1264, zNewFile))
											}
											runScript(tls, 0, 0, zNewScript, zNewFile)
											libsqlite3.Xsqlite3_free(tls, zNewScript)
											if g.FiTrace != 0 {
												logMessage(tls, __ccgo_ts+1392, libc.VaList(bp+1264, zNewFile))
											}
											libsqlite3.Xsqlite3_free(tls, zToDel)
										} else {
											/*
											 **  --print MESSAGE....
											 **
											 ** Output the remainder of the line to the log file
											 */
											if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1409) == 0 {
												jj2 = int32(7)
												for {
													if !(jj2 < len1 && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(zScript + uintptr(ii+jj2)))))) != 0) {
														break
													}
													goto _9
												_9:
													;
													jj2++
												}
												logMessage(tls, __ccgo_ts+1206, libc.VaList(bp+1264, len1-jj2, zScript+uintptr(ii)+uintptr(jj2)))
											} else {
												/*
												 **  --if EXPR
												 **
												 ** Skip forward to the next matching --endif or --else if EXPR is false.
												 */
												if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1415) == 0 {
													jj3 = int32(4)
													for {
														if !(jj3 < len1 && libc.Xisspace(tls, int32(uint8(*(*int8)(unsafe.Pointer(zScript + uintptr(ii+jj3)))))) != 0) {
															break
														}
														goto _10
													_10:
														;
														jj3++
													}
													pStmt = prepareSql(tls, __ccgo_ts+1418, libc.VaList(bp+1264, len1-jj3, zScript+uintptr(ii)+uintptr(jj3)))
													rc1 = libsqlite3.Xsqlite3_step(tls, pStmt)
													if rc1 != int32(SQLITE_ROW) || libsqlite3.Xsqlite3_column_int(tls, pStmt, 0) == 0 {
														ii += findEndif(tls, zScript+uintptr(ii)+uintptr(len1), int32(1), bp)
													}
													libsqlite3.Xsqlite3_finalize(tls, pStmt)
												} else {
													/*
													 **  --else
													 **
													 ** This command can only be encountered if currently inside an --if that
													 ** is true.  Skip forward to the next matching --endif.
													 */
													if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1430) == 0 {
														ii += findEndif(tls, zScript+uintptr(ii)+uintptr(len1), 0, bp)
													} else {
														/*
														 **  --endif
														 **
														 ** This command can only be encountered if currently inside an --if that
														 ** is true or an --else of a false if.  This is a no-op.
														 */
														if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1435) == 0 {
															/* no-op */
														} else {
															/*
															 **  --start CLIENT
															 **
															 ** Start up the given client.
															 */
															if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1441) == 0 && iClient == 0 {
																iNewClient = libc.Xatoi(tls, bp+1054)
																if iNewClient > 0 {
																	startClient(tls, iNewClient)
																}
															} else {
																/*
																 **  --wait CLIENT TIMEOUT
																 **
																 ** Wait until all tasks complete for the given client.  If CLIENT is
																 ** "all" then wait for all clients to complete.  Wait no longer than
																 ** TIMEOUT milliseconds (default 10,000)
																 */
																if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1447) == 0 && iClient == 0 {
																	if nArg >= int32(2) {
																		v11 = libc.Xatoi(tls, bp+1054+1*100)
																	} else {
																		v11 = int32(10000)
																	}
																	iTimeout = v11
																	libsqlite3.Xsqlite3_snprintf(tls, int32(1000), bp+54, __ccgo_ts+1452, libc.VaList(bp+1264, prevLine, zFilename))
																	waitForClient(tls, libc.Xatoi(tls, bp+1054), iTimeout, bp+54)
																} else {
																	/*
																	 **  --task CLIENT
																	 **     <task-content-here>
																	 **  --end
																	 **
																	 ** Assign work to a client.  Start the client if it is not running
																	 ** already.
																	 */
																	if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1467) == 0 && iClient == 0 {
																		iTarget = libc.Xatoi(tls, bp+1054)
																		iEnd = findEnd(tls, zScript+uintptr(ii)+uintptr(len1), bp)
																		if iTarget < 0 {
																			errorMessage(tls, __ccgo_ts+1472, libc.VaList(bp+1264, prevLine, zFilename, iTarget))
																		} else {
																			zTask = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+1206, libc.VaList(bp+1264, iEnd, zScript+uintptr(ii)+uintptr(len1)))
																			if nArg > int32(1) {
																				zTName = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+438, libc.VaList(bp+1264, bp+1054+1*100))
																			} else {
																				zTName = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+1509, libc.VaList(bp+1264, filenameTail(tls, zFilename), prevLine))
																			}
																			startClient(tls, iTarget)
																			runSql(tls, __ccgo_ts+1515, libc.VaList(bp+1264, iTarget, zTask, zTName))
																			libsqlite3.Xsqlite3_free(tls, zTask)
																			libsqlite3.Xsqlite3_free(tls, zTName)
																		}
																		iEnd += tokenLength(tls, zScript+uintptr(ii)+uintptr(len1)+uintptr(iEnd), bp)
																		len1 += iEnd
																		iBegin = ii + len1
																	} else {
																		/*
																		 **  --breakpoint
																		 **
																		 ** This command calls "test_breakpoint()" which is a routine provided
																		 ** as a convenient place to set a debugger breakpoint.
																		 */
																		if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1571) == 0 {
																			test_breakpoint(tls)
																		} else {
																			/*
																			 **  --show-sql-errors BOOLEAN
																			 **
																			 ** Turn display of SQL errors on and off.
																			 */
																			if libc.Xstrcmp(tls, bp+24, __ccgo_ts+1582) == 0 {
																				if nArg >= int32(1) {
																					v12 = libc.BoolInt32(!(booleanValue(tls, bp+1054) != 0))
																				} else {
																					v12 = int32(1)
																				}
																				g.FbIgnoreSqlErrors = v12
																			} else {
																				/* error */
																				errorMessage(tls, __ccgo_ts+1598, libc.VaList(bp+1264, prevLine, zFilename, bp+24))
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		ii += len1
	}
	if iBegin < ii {
		zSql1 = libsqlite3.Xsqlite3_mprintf(tls, __ccgo_ts+1206, libc.VaList(bp+1264, ii-iBegin, zScript+uintptr(iBegin)))
		runSql(tls, zSql1, 0)
		libsqlite3.Xsqlite3_free(tls, zSql1)
	}
	stringFree(tls, bp+8)
}

// C documentation
//
//	/*
//	** Look for a command-line option.  If present, return a pointer.
//	** Return NULL if missing.
//	**
//	** hasArg==0 means the option is a flag.  It is either present or not.
//	** hasArg==1 means the option has an argument.  Return a pointer to the
//	** argument.
//	*/
func findOption(tls *libc.TLS, azArg uintptr, pnArg uintptr, zOption uintptr, hasArg int32) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i, j, nArg, v2, v3 int32
	var z, zReturn uintptr
	_, _, _, _, _, _, _ = i, j, nArg, z, zReturn, v2, v3
	zReturn = uintptr(0)
	nArg = *(*int32)(unsafe.Pointer(pnArg))
	i = 0
	for {
		if !(i < nArg) {
			break
		}
		if i+hasArg >= nArg {
			break
		}
		z = *(*uintptr)(unsafe.Pointer(azArg + uintptr(i)*8))
		if int32(*(*int8)(unsafe.Pointer(z))) != int32('-') {
			goto _1
		}
		z++
		if int32(*(*int8)(unsafe.Pointer(z))) == int32('-') {
			if int32(*(*int8)(unsafe.Pointer(z + 1))) == 0 {
				break
			}
			z++
		}
		if libc.Xstrcmp(tls, z, zOption) == 0 {
			if hasArg != 0 && i == nArg-int32(1) {
				fatalError(tls, __ccgo_ts+1634, libc.VaList(bp+8, z))
			}
			if hasArg != 0 {
				zReturn = *(*uintptr)(unsafe.Pointer(azArg + uintptr(i+int32(1))*8))
			} else {
				zReturn = *(*uintptr)(unsafe.Pointer(azArg + uintptr(i)*8))
			}
			j = i + int32(1) + libc.BoolInt32(hasArg != 0)
			for j < nArg {
				v2 = i
				i++
				v3 = j
				j++
				*(*uintptr)(unsafe.Pointer(azArg + uintptr(v2)*8)) = *(*uintptr)(unsafe.Pointer(azArg + uintptr(v3)*8))
			}
			*(*int32)(unsafe.Pointer(pnArg)) = i
			return zReturn
		}
		goto _1
	_1:
		;
		i++
	}
	return zReturn
}

// C documentation
//
//	/* Print a usage message for the program and exit */
func usage(tls *libc.TLS, argv0 uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var __local_argv __builtin_va_list
	var __retval, i int32
	var zTail, v2, v4 uintptr
	_, _, _, _, _, _ = __local_argv, __retval, i, zTail, v2, v4
	zTail = argv0
	i = 0
	for {
		if !(*(*int8)(unsafe.Pointer(argv0 + uintptr(i))) != 0) {
			break
		}
		if int32(*(*int8)(unsafe.Pointer(argv0 + uintptr(i)))) == int32('/') || int32(*(*int8)(unsafe.Pointer(argv0 + uintptr(i)))) == int32('\\') {
			zTail = argv0 + uintptr(i) + uintptr(1)
		}
		goto _1
	_1:
		;
		i++
	}
	v2 = __ccgo_ts + 1682
	libc.VaList(bp, zTail)
	__local_argv = bp
	__retval = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(2)), v2, __local_argv)
	_ = __local_argv
	_ = __retval
	goto _3
_3:
	;
	v4 = __ccgo_ts + 1721
	__local_argv = bp
	__retval = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(2)), v4, __local_argv)
	_ = __local_argv
	_ = __retval
	goto _5
_5:
	;
	libc.Xexit(tls, int32(1))
}

// C documentation
//
//	/* Report on unrecognized arguments */
func unrecognizedArguments(tls *libc.TLS, argv0 uintptr, nArg int32, azArg uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var __local_argv __builtin_va_list
	var __retval, i int32
	var v1, v4, v6 uintptr
	_, _, _, _, _, _ = __local_argv, __retval, i, v1, v4, v6
	v1 = __ccgo_ts + 2293
	libc.VaList(bp, argv0)
	__local_argv = bp
	__retval = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(2)), v1, __local_argv)
	_ = __local_argv
	_ = __retval
	goto _2
_2:
	;
	i = 0
	for {
		if !(i < nArg) {
			break
		}
		v4 = __ccgo_ts + 2321
		libc.VaList(bp, *(*uintptr)(unsafe.Pointer(azArg + uintptr(i)*8)))
		__local_argv = bp
		__retval = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(2)), v4, __local_argv)
		_ = __local_argv
		_ = __retval
		goto _5
	_5:
		;
		goto _3
	_3:
		;
		i++
	}
	v6 = __ccgo_ts + 2325
	__local_argv = bp
	__retval = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(2)), v6, __local_argv)
	_ = __local_argv
	_ = __retval
	goto _7
_7:
	;
	libc.Xexit(tls, int32(1))
}

func main1(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	bp := tls.Alloc(352)
	defer tls.Free(352)
	var __local_argv, __local_argv1 __builtin_va_list
	var __retval, __retval1, i, iClient, iRep, iTimeout, iTmout, nRep, nTry, openFlags, rc, v16, v25, v26 int32
	var pStmt, zCOption, zClient, zJMode, zNRep, zTmout, zTrace, v1, v10, v13, v14, v19, v20, v22, v27, v29, v3, v32, v34, v6, v8 uintptr
	var v17 bool
	var _ /* n at bp+288 */ int32
	var _ /* taskId at bp+304 */ int32
	var _ /* zScript at bp+296 */ uintptr
	var _ /* zTaskName at bp+312 */ uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = __local_argv, __local_argv1, __retval, __retval1, i, iClient, iRep, iTimeout, iTmout, nRep, nTry, openFlags, pStmt, rc, zCOption, zClient, zJMode, zNRep, zTmout, zTrace, v1, v10, v13, v14, v16, v17, v19, v20, v22, v25, v26, v27, v29, v3, v32, v34, v6, v8
	openFlags = int32(SQLITE_OPEN_READWRITE)
	nRep = int32(1)
	iTmout = 0
	g.Fargv0 = *(*uintptr)(unsafe.Pointer(argv))
	g.FiTrace = int32(1)
	if argc < int32(2) {
		usage(tls, *(*uintptr)(unsafe.Pointer(argv)))
	}
	g.FzDbFile = *(*uintptr)(unsafe.Pointer(argv + 1*8))
	if strglob(tls, __ccgo_ts+2327, g.FzDbFile) != 0 {
		usage(tls, *(*uintptr)(unsafe.Pointer(argv)))
	}
	if libc.Bool(0 != 0) && libc.Xstrcmp(tls, libsqlite3.Xsqlite3_sourceid(tls), __ccgo_ts+2334) != 0 {
		v1 = __ccgo_ts + 2419
		libc.VaList(bp, libsqlite3.Xsqlite3_sourceid(tls), __ccgo_ts+2334)
		__local_argv = bp
		__retval = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(2)), v1, __local_argv)
		_ = __local_argv
		_ = __retval
		goto _2
	_2:
		;
		libc.Xexit(tls, int32(1))
	}
	*(*int32)(unsafe.Pointer(bp + 288)) = argc - int32(2)
	libsqlite3.Xsqlite3_snprintf(tls, int32(32), uintptr(unsafe.Pointer(&g))+64, __ccgo_ts+2479, libc.VaList(bp+328, int32(libc.XGetCurrentProcessId(tls))))
	zJMode = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2491, int32(1))
	zNRep = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2503, int32(1))
	if zNRep != 0 {
		nRep = libc.Xatoi(tls, zNRep)
	}
	if nRep < int32(1) {
		nRep = int32(1)
	}
	g.FzVfs = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2510, int32(1))
	zClient = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2514, int32(1))
	g.FzErrLog = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2521, int32(1))
	g.FzLog = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2528, int32(1))
	zTrace = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2532, int32(1))
	if zTrace != 0 {
		g.FiTrace = libc.Xatoi(tls, zTrace)
	}
	if findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2538, 0) != uintptr(0) {
		g.FiTrace = 0
	}
	zTmout = findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2544, int32(1))
	if zTmout != 0 {
		iTmout = libc.Xatoi(tls, zTmout)
	}
	g.FbSqlTrace = libc.BoolInt32(findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2552, 0) != uintptr(0))
	g.FbSync = libc.BoolInt32(findOption(tls, argv+uintptr(2)*8, bp+288, __ccgo_ts+2561, 0) != uintptr(0))
	if g.FzErrLog != 0 {
		g.FpErrLog = libc.Xfopen(tls, g.FzErrLog, __ccgo_ts+2566)
	} else {
		g.FpErrLog = libc.X__acrt_iob_func(tls, uint32(2))
	}
	if g.FzLog != 0 {
		g.FpLog = libc.Xfopen(tls, g.FzLog, __ccgo_ts+2566)
	} else {
		g.FpLog = libc.X__acrt_iob_func(tls, uint32(1))
	}
	libsqlite3.Xsqlite3_config(tls, int32(SQLITE_CONFIG_LOG), libc.VaList(bp+328, __ccgo_fp(sqlErrorCallback), 0))
	if zClient != 0 {
		iClient = libc.Xatoi(tls, zClient)
		if iClient < int32(1) {
			fatalError(tls, __ccgo_ts+2568, libc.VaList(bp+328, iClient))
		}
		libsqlite3.Xsqlite3_snprintf(tls, int32(32), uintptr(unsafe.Pointer(&g))+64, __ccgo_ts+2595, libc.VaList(bp+328, int32(libc.XGetCurrentProcessId(tls)), iClient))
	} else {
		nTry = 0
		if g.FiTrace > 0 {
			v3 = __ccgo_ts + 2611
			libc.VaList(bp, *(*uintptr)(unsafe.Pointer(argv)))
			__local_argv1 = bp
			__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v3, __local_argv1)
			_ = __local_argv1
			_ = __retval1
			goto _4
		_4:
			;
			i = int32(1)
			for {
				if !(i < argc) {
					break
				}
				v6 = __ccgo_ts + 2321
				libc.VaList(bp, *(*uintptr)(unsafe.Pointer(argv + uintptr(i)*8)))
				__local_argv1 = bp
				__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v6, __local_argv1)
				_ = __local_argv1
				_ = __retval1
				goto _7
			_7:
				;
				goto _5
			_5:
				;
				i++
			}
			v8 = __ccgo_ts + 2325
			__local_argv1 = bp
			__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v8, __local_argv1)
			_ = __local_argv1
			_ = __retval1
			goto _9
		_9:
			;
			v10 = __ccgo_ts + 2621
			__local_argv1 = bp
			__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v10, __local_argv1)
			_ = __local_argv1
			_ = __retval1
			goto _11
		_11:
			;
			i = 0
			for {
				v13 = libsqlite3.Xsqlite3_compileoption_get(tls, i)
				zCOption = v13
				if !(v13 != uintptr(0)) {
					break
				}
				v14 = __ccgo_ts + 2726
				libc.VaList(bp, zCOption)
				__local_argv1 = bp
				__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v14, __local_argv1)
				_ = __local_argv1
				_ = __retval1
				goto _15
			_15:
				;
				goto _12
			_12:
				;
				i++
			}
			libc.Xfflush(tls, libc.X__acrt_iob_func(tls, uint32(1)))
		}
		iClient = 0
		for {
			if nTry%int32(5) == int32(4) {
				if nTry > int32(5) {
					v19 = __ccgo_ts + 2739
				} else {
					v19 = __ccgo_ts + 1103
				}
				v20 = __ccgo_ts + 2746
				libc.VaList(bp, v19, g.FzDbFile)
				__local_argv1 = bp
				__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v20, __local_argv1)
				_ = __local_argv1
				_ = __retval1
				goto _21
			_21:
			}
			rc = libc.Xunlink(tls, g.FzDbFile)
			if rc != 0 && *(*int32)(unsafe.Pointer(libc.X_errno(tls))) == int32(ENOENT) {
				rc = 0
			}
			goto _18
		_18:
			;
			if v17 = rc != 0; v17 {
				nTry++
				v16 = nTry
			}
			if !(v17 && v16 < int32(60) && libsqlite3.Xsqlite3_sleep(tls, int32(1000)) > 0) {
				break
			}
		}
		if rc != 0 {
			fatalError(tls, __ccgo_ts+2775, libc.VaList(bp+328, g.FzDbFile, nTry))
		}
		openFlags |= int32(SQLITE_OPEN_CREATE)
	}
	rc = libsqlite3.Xsqlite3_open_v2(tls, g.FzDbFile, uintptr(unsafe.Pointer(&g))+24, openFlags, g.FzVfs)
	if rc != 0 {
		fatalError(tls, __ccgo_ts+2816, libc.VaList(bp+328, g.FzDbFile))
	}
	if iTmout > 0 {
		libsqlite3.Xsqlite3_busy_timeout(tls, g.Fdb, iTmout)
	}
	if zJMode != 0 {
		if libsqlite3.Xsqlite3_stricmp(tls, zJMode, __ccgo_ts+2833) == 0 || libsqlite3.Xsqlite3_stricmp(tls, zJMode, __ccgo_ts+2841) == 0 {
			v22 = __ccgo_ts + 2850
			libc.VaList(bp, zJMode)
			__local_argv1 = bp
			__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v22, __local_argv1)
			_ = __local_argv1
			_ = __retval1
			goto _23
		_23:
			;
			zJMode = __ccgo_ts + 2890
		}
		runSql(tls, __ccgo_ts+2897, libc.VaList(bp+328, zJMode))
	}
	if !(g.FbSync != 0) {
		trySql(tls, __ccgo_ts+2921, 0)
	}
	libsqlite3.Xsqlite3_enable_load_extension(tls, g.Fdb, int32(1))
	libsqlite3.Xsqlite3_busy_handler(tls, g.Fdb, __ccgo_fp(busyHandler), uintptr(0))
	libsqlite3.Xsqlite3_create_function(tls, g.Fdb, __ccgo_ts+2944, 0, int32(SQLITE_UTF8), uintptr(0), __ccgo_fp(vfsNameFunc), uintptr(0), uintptr(0))
	libsqlite3.Xsqlite3_create_function(tls, g.Fdb, __ccgo_ts+2952, int32(1), int32(SQLITE_UTF8), uintptr(0), __ccgo_fp(evalFunc), uintptr(0), uintptr(0))
	g.FiTimeout = int32(DEFAULT_TIMEOUT)
	if g.FbSqlTrace != 0 {
		libsqlite3.Xsqlite3_trace(tls, g.Fdb, __ccgo_fp(sqlTraceCallback), uintptr(0))
	}
	if iClient > 0 {
		if *(*int32)(unsafe.Pointer(bp + 288)) > 0 {
			unrecognizedArguments(tls, *(*uintptr)(unsafe.Pointer(argv)), *(*int32)(unsafe.Pointer(bp + 288)), argv+uintptr(2)*8)
		}
		if g.FiTrace != 0 {
			logMessage(tls, __ccgo_ts+2957, 0)
		}
		for int32(1) != 0 {
			*(*uintptr)(unsafe.Pointer(bp + 312)) = uintptr(0)
			rc = startScript(tls, iClient, bp+296, bp+304, bp+312)
			if rc == int32(SQLITE_DONE) {
				break
			}
			if g.FiTrace != 0 {
				logMessage(tls, __ccgo_ts+2970, libc.VaList(bp+328, *(*uintptr)(unsafe.Pointer(bp + 312)), *(*int32)(unsafe.Pointer(bp + 304))))
			}
			runScript(tls, iClient, *(*int32)(unsafe.Pointer(bp + 304)), *(*uintptr)(unsafe.Pointer(bp + 296)), *(*uintptr)(unsafe.Pointer(bp + 312)))
			if g.FiTrace != 0 {
				logMessage(tls, __ccgo_ts+2984, libc.VaList(bp+328, *(*uintptr)(unsafe.Pointer(bp + 312)), *(*int32)(unsafe.Pointer(bp + 304))))
			}
			finishScript(tls, iClient, *(*int32)(unsafe.Pointer(bp + 304)), 0)
			libsqlite3.Xsqlite3_free(tls, *(*uintptr)(unsafe.Pointer(bp + 312)))
			libsqlite3.Xsqlite3_sleep(tls, int32(10))
		}
		if g.FiTrace != 0 {
			logMessage(tls, __ccgo_ts+2996, 0)
		}
	} else {
		if *(*int32)(unsafe.Pointer(bp + 288)) == 0 {
			fatalError(tls, __ccgo_ts+3007, 0)
		}
		if *(*int32)(unsafe.Pointer(bp + 288)) > int32(1) {
			unrecognizedArguments(tls, *(*uintptr)(unsafe.Pointer(argv)), *(*int32)(unsafe.Pointer(bp + 288)), argv+uintptr(2)*8)
		}
		runSql(tls, __ccgo_ts+3031, 0)
		*(*uintptr)(unsafe.Pointer(bp + 296)) = readFile(tls, *(*uintptr)(unsafe.Pointer(argv + 2*8)))
		iRep = int32(1)
		for {
			if !(iRep <= nRep) {
				break
			}
			if g.FiTrace != 0 {
				logMessage(tls, __ccgo_ts+3467, libc.VaList(bp+328, *(*uintptr)(unsafe.Pointer(argv + 2*8)), iRep))
			}
			runScript(tls, 0, 0, *(*uintptr)(unsafe.Pointer(bp + 296)), *(*uintptr)(unsafe.Pointer(argv + 2*8)))
			if g.FiTrace != 0 {
				logMessage(tls, __ccgo_ts+3495, libc.VaList(bp+328, *(*uintptr)(unsafe.Pointer(argv + 2*8)), iRep))
			}
			goto _24
		_24:
			;
			iRep++
		}
		libsqlite3.Xsqlite3_free(tls, *(*uintptr)(unsafe.Pointer(bp + 296)))
		waitForClient(tls, 0, int32(2000), __ccgo_ts+3521)
		trySql(tls, __ccgo_ts+3541, 0)
		libsqlite3.Xsqlite3_sleep(tls, int32(10))
		g.FiTimeout = 0
		iTimeout = int32(1000)
		for {
			v25 = trySql(tls, __ccgo_ts+3570, 0)
			rc = v25
			if !((v25 == int32(SQLITE_BUSY) || rc == int32(SQLITE_ROW)) && iTimeout > 0) {
				break
			}
			libsqlite3.Xsqlite3_sleep(tls, int32(10))
			iTimeout -= int32(10)
		}
		libsqlite3.Xsqlite3_sleep(tls, int32(100))
		pStmt = prepareSql(tls, __ccgo_ts+3591, 0)
		iTimeout = int32(1000)
		for {
			v26 = libsqlite3.Xsqlite3_step(tls, pStmt)
			rc = v26
			if !(v26 == int32(SQLITE_BUSY) && iTimeout > 0) {
				break
			}
			libsqlite3.Xsqlite3_sleep(tls, int32(10))
			iTimeout -= int32(10)
		}
		if rc == int32(SQLITE_ROW) {
			g.FnError += libsqlite3.Xsqlite3_column_int(tls, pStmt, 0)
			g.FnTest += libsqlite3.Xsqlite3_column_int(tls, pStmt, int32(1))
		}
		libsqlite3.Xsqlite3_finalize(tls, pStmt)
	}
	libsqlite3.Xsqlite3_close(tls, g.Fdb)
	maybeClose(tls, g.FpLog)
	maybeClose(tls, g.FpErrLog)
	if iClient == 0 {
		v27 = __ccgo_ts + 3626
		libc.VaList(bp, g.FnError, g.FnTest)
		__local_argv1 = bp
		__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v27, __local_argv1)
		_ = __local_argv1
		_ = __retval1
		goto _28
	_28:
		;
		v29 = __ccgo_ts + 3662
		libc.VaList(bp, *(*uintptr)(unsafe.Pointer(argv)))
		__local_argv1 = bp
		__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v29, __local_argv1)
		_ = __local_argv1
		_ = __retval1
		goto _30
	_30:
		;
		i = int32(1)
		for {
			if !(i < argc) {
				break
			}
			v32 = __ccgo_ts + 2321
			libc.VaList(bp, *(*uintptr)(unsafe.Pointer(argv + uintptr(i)*8)))
			__local_argv1 = bp
			__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v32, __local_argv1)
			_ = __local_argv1
			_ = __retval1
			goto _33
		_33:
			;
			goto _31
		_31:
			;
			i++
		}
		v34 = __ccgo_ts + 2325
		__local_argv1 = bp
		__retval1 = libc.X__mingw_vfprintf(tls, libc.X__acrt_iob_func(tls, uint32(1)), v34, __local_argv1)
		_ = __local_argv1
		_ = __retval1
		goto _35
	_35:
	}
	return libc.BoolInt32(g.FnError > 0)
}

func main() {
	libc.Start(main1)
}

func __ccgo_fp(f interface{}) uintptr {
	type iface [2]uintptr
	return (*iface)(unsafe.Pointer(&f))[1]
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "%s%.*s\n\x00%s:ERROR: \x00%s:FATAL: \x00UPDATE client SET wantHalt=1;\x00%s: \x00main\x00timeout after %dms\x00[%.*s]\x00(info) %s\x00(errcode=%d) %s\x00%s\n%s\n\x00out of memory\x00 \x00nil\x00'\x00error(%d)\x00BEGIN IMMEDIATE\x00in startScript: %s\x00UPDATE counters SET nError=nError+%d, nTest=nTest+%d\x00SELECT 1 FROM client WHERE id=%d AND wantHalt\x00DELETE FROM client WHERE id=%d\x00COMMIT TRANSACTION;\x00SELECT script, id, name FROM task WHERE client=%d AND starttime IS NULL ORDER BY id LIMIT 1\x00%s\x00UPDATE task   SET starttime=strftime('%%Y-%%m-%%d %%H:%%M:%%f','now') WHERE id=%d;\x00Waited over 30 seconds with no work.  Giving up.\x00DELETE FROM client WHERE id=%d; COMMIT;\x00COMMIT\x00UPDATE task   SET endtime=strftime('%%Y-%%m-%%d %%H:%%M:%%f','now') WHERE id=%d;\x00INSERT OR IGNORE INTO client VALUES(%d,0)\x00%s \"%s\" --client %d --trace %d\x00%z --sqltrace\x00%z --sync\x00%z --vfs \"%s\"\x00system('%q')\x00CreateProcessA() fails with error code %lu\x00rb\x00cannot open \"%s\" for reading\x00--end\x00--endif\x00--else\x00--if\x00SELECT 1 FROM task WHERE client=%d   AND client IN (SELECT id FROM client)  AND endtime IS NULL\x00SELECT 1 FROM task WHERE client IN (SELECT id FROM client)   AND endtime IS NULL\x00\x00%stimeout waiting for client %d\x00%stimeout waiting for all clients\x00on\x00yes\x00off\x00no\x00unknown boolean: [%s]\x00%.*s\x00sleep\x00exit\x00testcase\x00finish\x00reset\x00match\x00line %d of %s:\nExpected [%.*s]\n     Got [%s]\x00glob\x00notglob\x00line %d of %s:\nExpected [%s]\n     Got [%s]\x00output\x00source\x00%.*s/%s\x00begin script [%s]\n\x00end script [%s]\n\x00print\x00if\x00SELECT %.*s\x00else\x00endif\x00start\x00wait\x00line %d of %s\n\x00task\x00line %d of %s: bad client number: %d\x00%s:%d\x00INSERT INTO task(client,script,name) VALUES(%d,'%q',%Q)\x00breakpoint\x00show-sql-errors\x00line %d of %s: unknown command --%s\x00command-line option \"--%s\" requires an argument\x00Usage: %s DATABASE ?OPTIONS? ?SCRIPT?\n\x00Options:\n   --errlog FILENAME           Write errors to FILENAME\n   --journalmode MODE          Use MODE as the journal_mode\n   --log FILENAME              Log messages to FILENAME\n   --quiet                     Suppress unnecessary output\n   --vfs NAME                  Use NAME as the VFS\n   --repeat N                  Repeat the test N times\n   --sqltrace                  Enable SQL tracing\n   --sync                      Enable synchronous disk writes\n   --timeout MILLISEC          Busy timeout is MILLISEC\n   --trace BOOLEAN             Enable or disable tracing\n\x00%s: unrecognized arguments:\x00 %s\x00\n\x00*.test\x002025-02-18 13:38:58 873d4e274b4988d260ba8354a9718324a1c26187a4ab4c1cc0227c03d0f1alt1\x00SQLite library and header mismatch\nLibrary: %s\nHeader:  %s\n\x00%05d.mptest\x00journalmode\x00repeat\x00vfs\x00client\x00errlog\x00log\x00trace\x00quiet\x00timeout\x00sqltrace\x00sync\x00a\x00illegal client number: %d\n\x00%05d.client%02d\x00BEGIN: %s\x00With SQLite 3.49.1 2025-02-18 13:38:58 873d4e274b4988d260ba8354a9718324a1c26187a4ab4c1cc0227c03d0f1alt1\n\x00-DSQLITE_%s\n\x00still \x00... %strying to unlink '%s'\n\x00unable to unlink '%s' after %d attempts\n\x00cannot open [%s]\x00persist\x00truncate\x00Changing journal mode to DELETE from %s\x00DELETE\x00PRAGMA journal_mode=%Q;\x00PRAGMA synchronous=OFF\x00vfsname\x00eval\x00start-client\x00begin %s (%d)\x00end %s (%d)\x00end-client\x00missing script filename\x00DROP TABLE IF EXISTS task;\nDROP TABLE IF EXISTS counters;\nDROP TABLE IF EXISTS client;\nCREATE TABLE task(\n  id INTEGER PRIMARY KEY,\n  name TEXT,\n  client INTEGER,\n  starttime DATE,\n  endtime DATE,\n  script TEXT\n);CREATE INDEX task_i1 ON task(client, starttime);\nCREATE INDEX task_i2 ON task(client, endtime);\nCREATE TABLE counters(nError,nTest);\nINSERT INTO counters VALUES(0,0);\nCREATE TABLE client(id INTEGER PRIMARY KEY, wantHalt);\n\x00begin script [%s] cycle %d\n\x00end script [%s] cycle %d\n\x00during shutdown...\n\x00UPDATE client SET wantHalt=1\x00SELECT 1 FROM client\x00SELECT nError, nTest FROM counters\x00Summary: %d errors out of %d tests\n\x00END: %s\x00"
